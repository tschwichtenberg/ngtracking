
/*
 * 
 *1. Add the script to your html
 *2. Add 'excel' to your module dependentcies
 *3 Add the following function to your controller linked to a button click
 *          tableId - is the id attribute of the table to export
 *          sheet is the mane to be sued for the workbook in excel
 */
 
angular.module('excel', [])
        .factory('Excel', function ($window) {
            var uri = 'data:application/vnd.ms-excel;base64,',
                    template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
                    base64 = function (s) {
                        return $window.btoa(unescape(encodeURIComponent(s)));
                    },
                    format = function (s, c) {
                        return s.replace(/{(\w+)}/g, function (m, p) {
                            return c[p];
                        })
                    };
            return {
                tableToExcel: function (tableId, workbookName) {

                    tablesToExcel($(tableId), workbookName, 'Excel')

                    /*
                     data-sheet-name
                     var table = $(tableId);
                     var cleanTable = table.clone();
                     cleanTable.find("*"of ).each(function () {
                     if ($(this).css("display") === "none") {
                     $(this).remove();
                     }
                     })
                     var ctx = {worksheet: worksheetName, table: cleanTable.html()};
                     var href = uri + base64(format(template, ctx));
                     return href;*/
                }
            };
        })


var tablesToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,',
            tmplWorkbookXML = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">'
            + '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>'
            + '<Styles>'
            + '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>'
            + '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>'
            + '<Style ss:ID="DateTime"><NumberFormat ss:Format="m/d/yy h:mm AM/PM"></NumberFormat></Style>'
            + '</Styles>'
            + '{worksheets}</Workbook>'
            , tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>'
            , tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>'
            , base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }
    , format = function (s, c) {
        return s.replace(/{(\w+)}/g, function (m, p) {
            return c[p];
        })
    }
    return function (tables, wbname, appname) {
        var ctx = "";
        var workbookXML = "";
        var worksheetsXML = "";
        var rowsXML = "";

        tables.each(function () {

            var v = $(this).clone();
            var wsname = $(this).attr("data-sheet-name")
            v.find("*").each(function () {
                if ($(this).css("display") === "none") {
                    $(this).remove();
                }
            });
            var v = v[0];
            for (var j = 0; j < v.rows.length; j++) {
                rowsXML += '<Row>'
                for (var k = 0; k < v.rows[j].cells.length; k++) {
                    var dataType = v.rows[j].cells[k].getAttribute("data-type");
                    var dataStyle = v.rows[j].cells[k].getAttribute("data-style");
                    var dataValue = v.rows[j].cells[k].getAttribute("data-value");
                    dataValue = (dataValue) ? dataValue : v.rows[j].cells[k].innerHTML;
                    var dataFormula = v.rows[j].cells[k].getAttribute("data-formula");
                    dataFormula = (dataFormula) ? dataFormula : (appname == 'Calc' && dataType == 'DateTime') ? dataValue : null;
                    ctx = {attributeStyleID: (dataStyle == 'Currency' || dataStyle == 'Date' || dataStyle == 'DateTime') ? ' ss:StyleID="' + dataStyle + '"' : ''
                        , nameType: (dataType == 'Number' || dataType == 'DateTime' || dataType == 'Boolean' || dataType == 'Error') ? dataType : 'String'
                        , data: (dataFormula) ? '' : dataValue
                        , attributeFormula: (dataFormula) ? ' ss:Formula="' + dataFormula + '"' : ''
                    };
                    rowsXML += format(tmplCellXML, ctx);
                }
                rowsXML += '</Row>'
            }
            ctx = {rows: rowsXML, nameWS: wsname || 'Sheet'};
            worksheetsXML += format(tmplWorksheetXML, ctx);
            rowsXML = "";
        });

        ctx = {created: (new Date()).getTime(), worksheets: worksheetsXML};
        workbookXML = format(tmplWorkbookXML, ctx);

        console.log(workbookXML);

        var link = document.createElement("A");
        link.href = uri + base64(workbookXML);
        link.download = wbname || 'Workbook.xls';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
})();
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


