// LMS Migration Tracking APP JS
// Tim Schwichtenberg
// tschwichtenberg@gmail.com

angular
	.module('ngTracking', ['ui.bootstrap', 'firebase', 'angular.filter', 'ui.router', 'excel'])
	.config(function($stateProvider) {
		
		$stateProvider
			.state('devry', {
				url: '/devry',
				templateUrl: 'components/institutions/devry.tpl.html',
				controller: 'trackingCtrl as vm'
			})
        
        .state('devryadmin', {
				url: '/devryadmin',
				templateUrl: 'components/institutions/devryadmin.tpl.html',
				controller: 'trackingCtrl as vm'
			})
        .state('devrytable', {
                url: '/devrytable',
				templateUrl: 'components/institutions/devrytable.tpl.html',
				controller: 'trackingCtrl as vm'
			})
        
        .state('chamberlain', {
				url: '/chamberlain',
				templateUrl: 'components/institutions/chamberlain.tpl.html',
				controller: 'chamberlainCtrl as vm'
			})
        
        .state('chamberlainadmin', {
				url: '/chamberlainadmin',
				templateUrl: 'components/institutions/chamberlainadmin.tpl.html',
				controller: 'chamberlainCtrl as vm'
			})
        
        .state('chamberlaintable', {
                url: '/chamberlaintable',
				templateUrl: 'components/institutions/chamberlaintable.tpl.html',
				controller: 'chamberlainCtrl as vm'
			})

	});


