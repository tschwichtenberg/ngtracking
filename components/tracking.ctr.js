// LMS Migration Tracking App Controller
// Tim Schwichtenberg
// tschwichtenberg@gmail.com

(function () {
    "use strict";

    angular
            .module('ngTracking')
            .controller('trackingCtrl', function ($scope, $state, $http, $location, $timeout, Excel) {


                var ref = firebase.database().ref();
				var vm = this;
                var instRef = {};
                var devryStatus = {};
				vm.changeInst = changeInst;
				//var statuses = {};
				//vm.statuses = statuses;
				
                function changeInst(inst) {
                    console.log(inst);
                    vm.devry = [];

                    if(instRef && instRef.off)
                        instRef.off();
                    if (devryStatus.off)
                        devryStatus.off();
                    instRef = ref.child(inst + '/courses');
                    devryStatus = ref.child(inst + '/statuses');
                    //change event
                    instRef.on('value', function (snapshot) {
                        $timeout(function () {
                            //get js object
                            var item = snapshot.val();
                            var existing = [];
                            //var toRemove = [];
                            //loop through newly retrieved course list 
                            batchData();
                            angular.forEach(item, function (v, i) {
                                //refrence the instance
                                item[i].index = i;
                                coursePercent(v);
                                existing.push(i);
                                var matched = false;
                                angular.forEach(vm.devry, function (course, ind) {
                                    //if (existing.indexOf(course.index) < 0)
                                        // toRemove.push(ind);
                                    //if new course matches existing course
			
                                    if (course.index === i) {
                                        matched = course;
                                    }
                                    ;
                                });
                                if (matched) {
                                    //set each key to new value
									
                                    angular.forEach(v, function (value, key) {
                                        if (key != "editing") {
										matched[key] = value;
										}
                                    });
                                } else {
                                    //add to course list
                                    vm.devry.push(v);
                                }
                            });
							

                        });
                    });
				}
                console.log('hi');
				changeInst('Devry');
					
				
				var devryTotal = {1:149, 2:139, 3:150, 4:140, 5:139, 6:171}
				var chamberlainTotal = {1:100}
                vm.devryTotal = devryTotal;
				vm.chamberlainTotal = chamberlainTotal;


                function batchData(course){
                    var batchDetails = [];
					vm.batchDetails = batchDetails;
					batchDetails[1] = {
						started : 0,
						completed: 0,
                        dateCompleted: '2/17/17',
                        signedOff: 0,
                        dateSignedOff: '2/24/17',
					}

					batchDetails[2] = {
						started : 0,
						completed: 0,
                        dateCompleted: '2/24/17',
                        signedOff: 0,
                        dateSignedOff: '3/3/17',
					}
					
				 	batchDetails[3] = {
						started : 0,
						completed: 0,
                        dateCompleted: '3/3/17',
                        signedOff: 0,
                        dateSignedOff: '3/10/17',
					}

					batchDetails[4] = {
						started : 0,
						completed: 0,
                        dateCompleted: '3/10/17',
                        signedOff: 0,
                        dateSignedOff: '3/17/17',
					}
					
					batchDetails[5] = {
						started : 0,
						completed: 0,
                        dateCompleted: '3/17/17',
                        signedOff: 0,
                        dateSignedOff: '3/24/17',
					}

					batchDetails[6] = {
						started : 0,
						completed: 0,
                        dateCompleted: '3/24/17',
                        signedOff: 0,
                        dateSignedOff: '3/31/17',
					}
                    
                }

                // Begin function to check for values and then calculate course percentages
                function coursePercent(course) {
				
					
					
				    vm.batchDetails[course.batch].started++

					
					
					var totalStartedPercent = (vm.batchDetails[vm.batchnumber].started / vm.devryTotal[vm.batchnumber]);
						
						

		
                    //Create vars, check to see if items exist and if not assign them a value
				
                    var migStat, cleanStat, techStat, qaStat, signStat;
					
					
                    if (course.migration && course.migration.migrationStatus) {
                        migStat = course.migration.migrationStatus;
                    } else {
                        migStat = 0;
                    }

                    if (course.cleaning && course.cleaning.cleaningStatus) {
                        cleanStat = course.cleaning.cleaningStatus;
                    } else {
                        cleanStat = 0;
                    }

                    if (course.technical && course.technical.technicalStatus) {
                        techStat = course.technical.technicalStatus;
                    } else {
                        techStat = 0;
                    }

                    if (course.qa && course.qa.qaStatus) {
                        qaStat = course.qa.qaStatus;
                    } else {
                        qaStat = 0;
                    }

                    if (course.signoff && course.signoff.signoffStatus) {
                        signStat = course.signoff.signoffStatus;
                    } else {
                        signStat = 0;
                    }
                    // End Checks

                    // Assign values to be used in course percentage
					course.migStat = migStat;
                    course.cleanStat = cleanStat;
                    course.techStat = techStat;
                    course.qaStat = qaStat;
                    course.signStat = signStat;

                    // Calculate completion percentage for overall migration completion (number in progressbar)
                    course.percentage = (course.migStat + course.cleanStat + course.techStat + course.qaStat + course.signStat) / 5;
					

					if (course.percentage == 80) {
							vm.batchDetails[course.batch].completed++;
						}			

                    
                                        

					if (course.percentage == 100) {
							vm.batchDetails[course.batch].signedOff++;
						}			


                    // Assign type based on completion percentage to change color of progressbar
                    var type;
                    var value = course.percentage;
                    if (value <= 19) {
                        type = 'danger';
                    } else if (value <= 79) {
                        type = 'warning';
                    } else {
                        type = 'success';
                    }
                    course.type = type;

                    return course;
                }
                // End function to check for values and calculate course percentages


                // Begin code to determine the sorting order of courses in each batch depending on checkbox selections
                var sortOrder = 'courseId';
                var sortByPercent = false;
                var cleanFilter;
                vm.sortByPercent = sortByPercent;
                vm.sortOrder = sortOrder;
                vm.cleanFilter = cleanFilter

                vm.setSortOrder = setSortOrder;
                function setSortOrder() {
                    if (vm.sortByPercent) {
                        vm.sortOrder = 'percentage';
                    } else {
                        vm.sortOrder = 'courseId';
                    }
                }

                // End sorting code

                // Batchnumber 
                var batchnumber = 1;
                vm.batchnumber = batchnumber;
    

                //Functionality to edit assigned

                vm.editAssigned = editAssigned;
                function editAssigned(course) {
                    course.editing = true;
                    vm.course = course;
                }

                vm.saveEdit = saveEdit;

                function saveEdit(course) {

                    instRef.child(course.index).child("migration").child("migrationAssigned").set(course.migration.migrationAssigned);
					
					if (course.cleaning.cleaningAssigned === undefined) {
                            course.cleaning.cleaningAssigned = '';
					}

                    instRef.child(course.index).child("cleaning").child("cleaningAssigned").set(course.cleaning.cleaningAssigned);
					
					if (course.qa.qaAssigned === undefined) {
                            course.qa.qaAssigned = '';
					}

                    instRef.child(course.index).child("qa").child("qaAssigned").set(course.qa.qaAssigned);
					
					if (course.technical.technicalAssigned === undefined) {
                            course.technical.technicalAssigned = '';
					}

                    instRef.child(course.index).child("technical").child("technicalAssigned").set(course.technical.technicalAssigned);
					
					if (course.signoff.signoffAssigned === undefined) {
                            course.signoff.signoffAssigned = '';
					}
					
                    instRef.child(course.index).child("signoff").child("signoffAssigned").set(course.signoff.signoffAssigned);

                    console.log('Saved');

                    course.editing = false;

                }


                //Begin migration phase completion functionality
                vm.migComp = migComp;
                function migComp(course) {
                    vm.course = course;
                    if (course.migration.migrationDate === undefined || course.migration.migrationDate === null) {
                        course.migComplete = true;
                    } else {
                            course.migComplete = false;
                        }
                    }
                


                vm.migComplete = migComplete;
                function migComplete(course) {
                   	instRef.child(course.index).child("migration").child("migrationDate").set(dateFormat('mediumDate')); 
					instRef.child(course.index).child("migration").child("migrationStatus").set(100);
                    instRef.child(course.index).child("migComplete").set(false);
                }

                vm.migIncomplete = migIncomplete;
                function migIncomplete(course) {
                    instRef.child(course.index).child("migration").child("migrationDate").set(''); 
					instRef.child(course.index).child("migration").child("migrationStatus").set(0);
                    instRef.child(course.index).child("migComplete").set(true);
                }

                //Begin cleaning phase completion functionality
                vm.cleanComp = cleanComp;
                function cleanComp(course) {
                    vm.course = course;
                    if (course.cleaning && course.cleaning.cleaningAssigned === undefined) {
                        var cleanAssign = '';
                        course.cleaning.cleaningAssigned = cleanAssign;
                    }
                    if (course.cleaning.cleaningDate === undefined || course.cleaning.cleaningDate === null) {
                        course.cleanComplete = true;
                    } else if (course.cleaning.cleaningDate) {
                        if (course.cleaning.cleaningDate == "") {
                            course.cleanComplete = true;
                        } else {
                            course.cleanComplete = false;
                        }
                    }
                }

                vm.cleanComplete = cleanComplete;
                function cleanComplete(course) {
					instRef.child(course.index).child("cleaning").child("cleaningDate").set(dateFormat('mediumDate')); 
					instRef.child(course.index).child("cleaning").child("cleaningStatus").set(100);
                    instRef.child(course.index).child("cleanComplete").set(false);
                }

                vm.cleanIncomplete = cleanIncomplete;
                function cleanIncomplete(course) {
                    instRef.child(course.index).child("cleaning").child("cleaningDate").set(''); 
					instRef.child(course.index).child("cleaning").child("cleaningStatus").set(0);
                    instRef.child(course.index).child("cleanComplete").set(true);
                }

                //Begin technical phase completion functionality
               vm.techComp = techComp;
                function techComp(course) {
                    vm.course = course;
                    if (course.technical && course.technical.technicalAssigned === undefined) {
                        var techAssign = '';
                        course.technical.technicalAssigned = techAssign;
                    }
                    if (course.technical.technicalDate === undefined || course.technical.technicalDate === null) {
                        course.techComplete = true;
                    } else if (course.technical.technicalDate) {
                        if (course.technical.technicalDate == "") {
                            course.techComplete = true;
                        } else {
                            course.techComplete = false;
                        }
                    }
                }

                vm.techComplete = techComplete;
                function techComplete(course) {
					instRef.child(course.index).child("technical").child("technicalDate").set(dateFormat('mediumDate')); 
					instRef.child(course.index).child("technical").child("technicalStatus").set(100);
					instRef.child(course.index).child("techComplete").set(false);
                }

                vm.techIncomplete = techIncomplete;
                function techIncomplete(course) {
					instRef.child(course.index).child("technical").child("technicalDate").set(''); 
					instRef.child(course.index).child("technical").child("technicalStatus").set(0);
					instRef.child(course.index).child("techComplete").set(true);		
                }

                //Begin QA phase completion functionality
                vm.qaComp = qaComp;
                function qaComp(course) {
                    vm.course = course;
                    if (course.qa && course.qa.qaAssigned === undefined) {
                        var qaAssign = '';
                        course.qa.qaAssigned = qaAssign;
                    }
                    if (course.qa.qaDate === undefined || course.qa.qaDate === null) {
                        course.qaComplete = true;
                    } else if (course.qa.qaDate) {
                        if (course.qa.qaDate == "") {
                            course.qaComplete = true;
                        } else {
                            course.qaComplete = false;
                        }
                    }
                }

                vm.qaComplete = qaComplete;
                function qaComplete(course) {
					instRef.child(course.index).child("qa").child("qaDate").set(dateFormat('mediumDate')); 
					instRef.child(course.index).child("qa").child("qaStatus").set(100);
                    instRef.child(course.index).child("qaComplete").set(false);

                }

                vm.qaIncomplete = qaIncomplete;
                function qaIncomplete(course) {
					instRef.child(course.index).child("qa").child("qaDate").set(''); instRef.child(course.index).child("qa").child("qaStatus").set(0);
                    instRef.child(course.index).child("qaComplete").set(true);	
                }


                //Begin signoff phase completion functionality
               /*vm.signComp = signComp;
                function signComp(course) {
                    vm.course = course;
                    if (course.signoff === undefined) {
                        var signoff = [];
                        course.signoff = signoff;
                        if (course.signoff.signoffAssigned === undefined) {
                            var signoffAssign = '';
                            course.signoff.signoffAssigned = signoffAssign;
                        }
                    } 
                    if (course.signoff.singoffDate === null) {
                        course.signComplete = true;
                    } else if (course.signoff.signoffDate) {
                        if (course.signoff.signoffDate == "") {
                            course.signComplete = true;
                        } else {
                            course.signComplete = false;
                        }
                    }
                }*/
        
                vm.signComp = signComp;
                function signComp(course) {
                    vm.course = course;
                    if (course.signoff === undefined){
                        var signoff = [];
                        course.signoff = signoff;
                        course.signComplete = true;
                    } else if (course.signoff && course.signoff.signoffAssigned === undefined) {
                        var signAssign = '';
                        course.signoff.signoffAssigned = signAssign;
                        course.signComplete = true;
                    }
                    if (course.signoff.signoffDate === undefined || course.signoff.signoffDate === null) {
                        course.signComplete = true;
                    } else if (course.signoff.signoffDate) {
                        if (course.signoff.signoffDate == "") {
                            course.signComplete = true;
                        } else {
                            course.signComplete = false;
                        }
                    }
                }

                vm.signComplete = signComplete;
                function signComplete(course) {
					instRef.child(course.index).child("signoff").child("signoffDate").set(dateFormat('mediumDate')); 
					instRef.child(course.index).child("signoff").child("signoffStatus").set(100);
                    instRef.child(course.index).child("signComplete").set(false);
                }

                vm.signIncomplete = signIncomplete;
				function signIncomplete(course){
					instRef.child(course.index).child("signoff").child("signoffDate").set(''); 
					instRef.child(course.index).child("signoff").child("signoffStatus").set(0);
					instRef.child(course.index).child("signComplete").set(true);	
                }
                
                vm.exportToExcel = exportToExcel;
                function exportToExcel(tableId, sheet) { // ex: '#my-table'
                    Excel.tableToExcel(tableId, sheet);
                }


            })

    
    
    //
    //
    //
    //    End Devry Controller, Begin Chamberlain Controller
    //
    //
    //
    
    
    
    
                .controller('chamberlainCtrl', function ($scope, $state, $http, $location, $timeout, Excel) {


                var ref = firebase.database().ref();
				var vm = this;
                var instRef = {};
                var devryStatus = {};
				vm.changeInst = changeInst;
				//var statuses = {};
				//vm.statuses = statuses;
				
                function changeInst(inst) {
                    console.log(inst);
                    vm.devry = [];

                    if(instRef && instRef.off)
                        instRef.off();
                    if (devryStatus.off)
                        devryStatus.off();
                    instRef = ref.child(inst + '/courses');
                    devryStatus = ref.child(inst + '/statuses');
                    //change event
                    instRef.on('value', function (snapshot) {
                        $timeout(function () {
                            //get js object
                            var item = snapshot.val();
                            var existing = [];
                            //var toRemove = [];
                            //loop through newly retrieved course list
                            batchData();
                            angular.forEach(item, function (v, i) {
                                //refrence the instance
                                item[i].index = i;
                                coursePercent(v);
                                existing.push(i);
                                var matched = false;
                                angular.forEach(vm.devry, function (course, ind) {
                                    //if (existing.indexOf(course.index) < 0)
                                        // toRemove.push(ind);
                                    //if new course matches existing course
			
                                    if (course.index === i) {
                                        matched = course;
                                    }
                                    ;
                                });
                                if (matched) {
                                    //set each key to new value
									
                                    angular.forEach(v, function (value, key) {
                                        if (key != "editing") {
										matched[key] = value;
										}
                                    });
                                } else {
                                    //add to course list
                                    vm.devry.push(v);
                                }
                            });
							

                        });
                    });
				}
				changeInst('Chamberlain');
					
				
				var batchTotal = {1:1, 2:1, 3:1, 4:1, 5:1, 6:1}
                vm.batchTotal = batchTotal;


        
                function batchData(course) {    
                    var batchDetails = [];
					vm.batchDetails = batchDetails;

					batchDetails[1] = {
						started : 0,
						completed: 0,
                        dateCompleted: '1/1/2017',
                        signedOff: 0,
                        dateSignedOff: '1/1/2017',
						program: 'MSN',
					}

					batchDetails[2] = {
						started : 0,
						completed: 0,
                        dateCompleted: '1/1/2017',
                        signedOff: 0,
                        dateSignedOff: '1/1/2017',
						program: 'PL',
					}
					
				 	batchDetails[3] = {
						started : 0,
						completed: 0,
                        dateCompleted: '1/1/2017',
                        signedOff: 0,
                        dateSignedOff: '1/1/2017',
					}

					batchDetails[4] = {
						started : 0,
						completed: 0,
                        dateCompleted: '1/1/2017',
                        signedOff: 0,
                        dateSignedOff: '1/1/2017',
					}
					
					batchDetails[5] = {
						started : 0,
						completed: 0,
                        dateCompleted: '1/1/2017',
                        signedOff: 0,
                        dateSignedOff: '1/1/2017',
					}

					batchDetails[6] = {
						started : 0,
						completed: 0,
                        dateCompleted: '1/1/2017',
                        signedOff: 0,
                        dateSignedOff: '1/1/2017',
					}  
                }


                
                // Begin function to check for values and then calculate course percentages
                function coursePercent(course) {
					
					
				    	

				    vm.batchDetails[course.batch].started++

					
					
					var totalStartedPercent = (vm.batchDetails[vm.batchnumber].started / vm.batchTotal[vm.batchnumber]);
						
						

		
                    //Create vars, check to see if items exist and if not assign them a value
				
                    var migStat, cleanStat, techStat, qaStat, signStat;
					
					
                    if (course.migration && course.migration.migrationStatus) {
                        migStat = course.migration.migrationStatus;
                    } else {
                        migStat = 0;
                    }

                    if (course.cleaning && course.cleaning.cleaningStatus) {
                        cleanStat = course.cleaning.cleaningStatus;
                    } else {
                        cleanStat = 0;
                    }

                    if (course.technical && course.technical.technicalStatus) {
                        techStat = course.technical.technicalStatus;
                    } else {
                        techStat = 0;
                    }

                    if (course.qa && course.qa.qaStatus) {
                        qaStat = course.qa.qaStatus;
                    } else {
                        qaStat = 0;
                    }

                    if (course.signoff && course.signoff.signoffStatus) {
                        signStat = course.signoff.signoffStatus;
                    } else {
                        signStat = 0;
                    }
                    // End Checks

                    // Assign values to be used in course percentage
					course.migStat = migStat;
                    course.cleanStat = cleanStat;
                    course.techStat = techStat;
                    course.qaStat = qaStat;
                    course.signStat = signStat;

                    // Calculate completion percentage for overall migration completion (number in progressbar)
                    course.percentage = (course.migStat + course.cleanStat + course.techStat + course.qaStat + course.signStat) / 5;
					

					if (course.percentage == 80) {
							vm.batchDetails[course.batch].completed++;
				    }			

                    

					if (course.percentage == 100) {
							vm.batchDetails[course.batch].signedOff++;
				    }			


                    // Assign type based on completion percentage to change color of progressbar
                    var type;
                    var value = course.percentage;
                    if (value <= 19) {
                        type = 'danger';
                    } else if (value <= 79) {
                        type = 'warning';
                    } else {
                        type = 'success';
                    }
                    course.type = type;

                    return course;
                }
                // End function to check for values and calculate course percentages


                // Begin code to determine the sorting order of courses in each batch depending on checkbox selections
                var sortOrder = 'courseId';
                var sortByPercent = false;
                var cleanFilter;
                vm.sortByPercent = sortByPercent;
                vm.sortOrder = sortOrder;
                vm.cleanFilter = cleanFilter

                vm.setSortOrder = setSortOrder;
                function setSortOrder() {
                    if (vm.sortByPercent) {
                        vm.sortOrder = 'percentage';
                    } else {
                        vm.sortOrder = 'courseId';
                    }
                }

                // End sorting code

                // Batchnumber 
                var batchnumber = 1;
                vm.batchnumber = batchnumber;
    

                //Functionality to edit assigned

                vm.editAssigned = editAssigned;
                function editAssigned(course) {
                    course.editing = true;
                    vm.course = course;
                }

                vm.saveEdit = saveEdit;

                function saveEdit(course) {

                    instRef.child(course.index).child("migration").child("migrationAssigned").set(course.migration.migrationAssigned);
					
					if (course.cleaning.cleaningAssigned === undefined) {
                            course.cleaning.cleaningAssigned = '';
					}

                    instRef.child(course.index).child("cleaning").child("cleaningAssigned").set(course.cleaning.cleaningAssigned);
					
					if (course.qa.qaAssigned === undefined) {
                            course.qa.qaAssigned = '';
					}

                    instRef.child(course.index).child("qa").child("qaAssigned").set(course.qa.qaAssigned);
					
					if (course.technical.technicalAssigned === undefined) {
                            course.technical.technicalAssigned = '';
					}

                    instRef.child(course.index).child("technical").child("technicalAssigned").set(course.technical.technicalAssigned);
					
					if (course.signoff.signoffAssigned === undefined) {
                            course.signoff.signoffAssigned = '';
					}
					
                    instRef.child(course.index).child("signoff").child("signoffAssigned").set(course.signoff.signoffAssigned);

                    console.log('Saved');

                    course.editing = false;

                }


                //Begin migration phase completion functionality
                vm.migComp = migComp;
                function migComp(course) {
                    vm.course = course;
					course.migComplete = true;
                    if (course.migration.migrationDate === undefined || course.migration.migrationDate === null) {
                        course.migComplete = true;
                    } else if (course.migration.migrationDate == ""){
						course.migComplete = true;
					} else {
                            course.migComplete = false;
                        }
                    }
                


                vm.migComplete = migComplete;
                function migComplete(course) {
                   	instRef.child(course.index).child("migration").child("migrationDate").set(dateFormat('mediumDate')); 
					instRef.child(course.index).child("migration").child("migrationStatus").set(100);
                    instRef.child(course.index).child("migComplete").set(false);
                }

                vm.migIncomplete = migIncomplete;
                function migIncomplete(course) {
                    instRef.child(course.index).child("migration").child("migrationDate").set(''); 
					instRef.child(course.index).child("migration").child("migrationStatus").set(0);
                    instRef.child(course.index).child("migComplete").set(true);
                }

                //Begin cleaning phase completion functionality
                vm.cleanComp = cleanComp;
                function cleanComp(course) {
                    vm.course = course;
                    if (course.cleaning && course.cleaning.cleaningAssigned === undefined) {
                        var cleanAssign = '';
                        course.cleaning.cleaningAssigned = cleanAssign;
                    }
                    if (course.cleaning.cleaningDate === undefined || course.cleaning.cleaningDate === null) {
                        course.cleanComplete = true;
                    } else if (course.cleaning.cleaningDate) {
                        if (course.cleaning.cleaningDate == "") {
                            course.cleanComplete = true;
                        } else {
                            course.cleanComplete = false;
                        }
                    }
                }

                vm.cleanComplete = cleanComplete;
                function cleanComplete(course) {
					instRef.child(course.index).child("cleaning").child("cleaningDate").set(dateFormat('mediumDate')); 
					instRef.child(course.index).child("cleaning").child("cleaningStatus").set(100);
                    instRef.child(course.index).child("cleanComplete").set(false);
                }

                vm.cleanIncomplete = cleanIncomplete;
                function cleanIncomplete(course) {
                    instRef.child(course.index).child("cleaning").child("cleaningDate").set(''); 
					instRef.child(course.index).child("cleaning").child("cleaningStatus").set(0);
                    instRef.child(course.index).child("cleanComplete").set(true);
                }

                //Begin technical phase completion functionality
               vm.techComp = techComp;
                function techComp(course) {
                    vm.course = course;
                    if (course.technical && course.technical.technicalAssigned === undefined) {
                        var techAssign = '';
                        course.technical.technicalAssigned = techAssign;
                    }
                    if (course.technical.technicalDate === undefined || course.technical.technicalDate === null) {
                        course.techComplete = true;
                    } else if (course.technical.technicalDate) {
                        if (course.technical.technicalDate == "") {
                            course.techComplete = true;
                        } else {
                            course.techComplete = false;
                        }
                    }
                }

                vm.techComplete = techComplete;
                function techComplete(course) {
					instRef.child(course.index).child("technical").child("technicalDate").set(dateFormat('mediumDate')); 
					instRef.child(course.index).child("technical").child("technicalStatus").set(100);
					instRef.child(course.index).child("techComplete").set(false);
                }

                vm.techIncomplete = techIncomplete;
                function techIncomplete(course) {
					instRef.child(course.index).child("technical").child("technicalDate").set(''); 
					instRef.child(course.index).child("technical").child("technicalStatus").set(0);
					instRef.child(course.index).child("techComplete").set(true);		
                }

                //Begin QA phase completion functionality
                vm.qaComp = qaComp;
                function qaComp(course) {
                    vm.course = course;
                    if (course.qa && course.qa.qaAssigned === undefined) {
                        var qaAssign = '';
                        course.qa.qaAssigned = qaAssign;
                    }
                    if (course.qa.qaDate === undefined || course.qa.qaDate === null) {
                        course.qaComplete = true;
                    } else if (course.qa.qaDate) {
                        if (course.qa.qaDate == "") {
                            course.qaComplete = true;
                        } else {
                            course.qaComplete = false;
                        }
                    }
                }

                vm.qaComplete = qaComplete;
                function qaComplete(course) {
					instRef.child(course.index).child("qa").child("qaDate").set(dateFormat('mediumDate')); 
					instRef.child(course.index).child("qa").child("qaStatus").set(100);
                    instRef.child(course.index).child("qaComplete").set(false);

                }

                vm.qaIncomplete = qaIncomplete;
                function qaIncomplete(course) {
					instRef.child(course.index).child("qa").child("qaDate").set(''); instRef.child(course.index).child("qa").child("qaStatus").set(0);
                    instRef.child(course.index).child("qaComplete").set(true);	
                }


                //Begin signoff phase completion functionality
               /*vm.signComp = signComp;
                function signComp(course) {
                    vm.course = course;
                    if (course.signoff === undefined) {
                        var signoff = [];
                        course.signoff = signoff;
                        if (course.signoff.signoffAssigned === undefined) {
                            var signoffAssign = '';
                            course.signoff.signoffAssigned = signoffAssign;
                        }
                    } 
                    if (course.signoff.singoffDate === null) {
                        course.signComplete = true;
                    } else if (course.signoff.signoffDate) {
                        if (course.signoff.signoffDate == "") {
                            course.signComplete = true;
                        } else {
                            course.signComplete = false;
                        }
                    }
                }*/
        
                vm.signComp = signComp;
                function signComp(course) {
                    vm.course = course;
                    if (course.signoff === undefined){
                        var signoff = [];
                        course.signoff = signoff;
                        course.signComplete = true;
                    } else if (course.signoff && course.signoff.signoffAssigned === undefined) {
                        var signAssign = '';
                        course.signoff.signoffAssigned = signAssign;
                        course.signComplete = true;
                    }
                    if (course.signoff.signoffDate === undefined || course.signoff.signoffDate === null) {
                        course.signComplete = true;
                    } else if (course.signoff.signoffDate) {
                        if (course.signoff.signoffDate == "") {
                            course.signComplete = true;
                        } else {
                            course.signComplete = false;
                        }
                    }
                }

                vm.signComplete = signComplete;
                function signComplete(course) {
					instRef.child(course.index).child("signoff").child("signoffDate").set(dateFormat('mediumDate')); 
					instRef.child(course.index).child("signoff").child("signoffStatus").set(100);
                    instRef.child(course.index).child("signComplete").set(false);
                }

                vm.signIncomplete = signIncomplete;
				function signIncomplete(course){
					instRef.child(course.index).child("signoff").child("signoffDate").set(''); 
					instRef.child(course.index).child("signoff").child("signoffStatus").set(0);
					instRef.child(course.index).child("signComplete").set(true);	
                }
                
                vm.exportToExcel = exportToExcel;
                function exportToExcel(tableId, sheet) { // ex: '#my-table'
                    Excel.tableToExcel(tableId, sheet);
                }


            })

            .filter('filterClean', [function () {

                    return function (courses, cleanFilter) {

                        if (cleanFilter == true) {
                            var filtered = [];
                            angular.forEach(courses, function (course) {
                                if (course.percentage && course.percentage.toFixed() == 20 && course.cleaning.cleaningAssigned == '') {
                                    filtered.push(course);
                                }
                            });
                            return filtered;
                        } else {
                            return courses;
                        }
                    };
                }])

            .filter('filterTech', [function () {

                    return function (courses, techFilter) {

                        if (techFilter == true) {
                            var filtered = [];
                            angular.forEach(courses, function (course) {
                                if (course.percentage && course.percentage.toFixed() == 40 && course.technical.technicalAssigned == '') {
                                    filtered.push(course);
                                }
                            });
                            return filtered;
                        } else {
                            return courses;
                        }
                    };
                }])


            .filter('filterQa', [function () {

                    return function (courses, qaFilter) {

                        if (qaFilter == true) {
                            var filtered = [];
                            angular.forEach(courses, function (course) {
                                if (course.percentage && course.percentage.toFixed() == 60 && course.qa.qaAssigned == '') {
                                    filtered.push(course);
                                }
                            });
                            return filtered;
                        } else {
                            return courses;
                        }
                    };
                }])




            .filter('filterSignoff', [function () {

                    return function (courses, signoffFilter) {

                        if (signoffFilter == true) {
                            var filtered = [];
                            angular.forEach(courses, function (course) {
                                if (course.percentage && course.percentage.toFixed() == 80 && course.signoff.signoffAssigned == '') {
                                    filtered.push(course);
                                }
                            });
                            return filtered;
                        } else {
                            return courses;
                        }
                    };
                }])

            

})();
